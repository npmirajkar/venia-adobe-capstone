import React from "react";
import "./Terms.scss";

const Terms = () => {
    return (<>
        <main className="main-terms">
            <p className="use-terms">Terms of Use</p>
            <p className="policy-terms">Privacy Policy</p>
        </main>
    </>)
}

export default Terms;