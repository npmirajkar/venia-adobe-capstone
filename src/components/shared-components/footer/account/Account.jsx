import React from "react";
import "./Account.scss";

const Account = () => {
    return (<>
        <main className="main-account">
            <p className="title-account">Account</p>
            <p className="sign-in-account">Sign In</p>
            <p className="register-account">Register</p>
            <p className="order-status-account">Order Status</p>
            <p className="return-account">Returns</p>
        </main>
    </>)
}

export default Account;